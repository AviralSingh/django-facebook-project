from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user  = models.OneToOneField(User,on_delete=models.CASCADE)
    gender = models.CharField(max_length=10)
    date_of_birth = models.DateField()
    profile_image = models.FileField(upload_to='media/',null=True,blank=True,default='/home/aviral/Desktop/facebook/src/core/static/images/user.png')
    cover_image=models.FileField(upload_to='media/',null=True,blank=True,default='/home/aviral/Desktop/facebook/src/core/static/images/white.png')
    bio = models.CharField(max_length=500,null=True,blank=True)
    hometown = models.CharField(max_length=500,null=True,blank=True)
    city = models.CharField(max_length=500,null=True,blank=True)
    school = models.CharField(max_length=500,null=True,blank=True)
    workplace = models.CharField(max_length=500,blank=True,null=True)
    phone_number = models.IntegerField(blank=True,null=True)
    language=models.CharField(max_length=100,null=True,blank=True)
class Post(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    description = models.CharField(max_length=2000,null=True,blank=True)
    likes = models.IntegerField(null=True,blank=True,default=0)
    comment=models.IntegerField(default=0)
    media_file = models.FileField(upload_to='media/',null=True,blank=True)
    posted = models.DateTimeField(auto_now_add=True)
    
class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    created=models.DateTimeField(auto_now_add=True)
    body = models.CharField(max_length=3000)

class Likes(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='post_likes')
    
class Friends(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,related_name='friend')
    friends = models.ManyToManyField(to=User,related_name='friends')
    def create_follow(sender,instance,created,**kwargs):
        if created:
            user = Friends(user=instance)
            user.save()
    models.signals.post_save.connect(create_follow,sender=User)